﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Management;
using System.Threading.Tasks;
using Coypu;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using UITests.SiteModel.Base;


namespace UITests.SiteModel.Session
{
    public class Navigator
    {
        public enum Driver
        {
            Firefox,
            Chrome,
            PhantomJS
        }

        private readonly Driver driver;

        public Navigator(Driver driver)
        {
            this.driver = driver;

            switch (driver)
            {
                case Driver.Firefox:
                    Browser = new BrowserSession(new SessionConfiguration
                    {
                        Driver = typeof (CustomFirefoxProfile)
                    });
                    break;
                case Driver.Chrome:
                    Browser = new BrowserSession(new SessionConfiguration
                    {
                        Browser = Coypu.Drivers.Browser.Chrome,
                        Timeout = TimeSpan.FromSeconds(6)
                    });
                    break;
                case Driver.PhantomJS:
                    Browser = new BrowserSession(new SessionConfiguration
                    {
                        Browser = Coypu.Drivers.Browser.PhantomJS,
                        Timeout = TimeSpan.FromSeconds(6)
                    });
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public BrowserSession Browser { get; set; }

        public IWebDriver Native
        {
            get
            {
                switch (driver)
                {
                    case Driver.Firefox:
                        return (FirefoxDriver) Browser.Native;
                    case Driver.Chrome:
                        return (ChromeDriver) Browser.Native;
                    case Driver.PhantomJS:
                        return (PhantomJSDriver) Browser.Native;
                    default:
                        return (RemoteWebDriver) Browser.Native;
                }
            }
        }

        public T Visit<T>() where T : PageObject
        {
            var page = (T) Activator.CreateInstance(typeof (T), Browser);
            return Visit(page);
        }

        public T Visit<T>(T page) where T : PageObject
        {
            var url = (string) page.GetType().GetField("URL").GetValue(null);
            if (url != null)
            {
                Browser.Visit(url);
            }
            else
            {
                throw new Exception("No URL is available on the page " + typeof (T).FullName);
            }
            return page;
        }

        public void Visit(string url)
        {
            if (url != null)
            {
                Browser.Visit(url);
            }
        }

        public T GoTo<T>() where T : PageObject
        {
            var page = (T) Activator.CreateInstance(typeof (T), Browser);
            return page;
        }

        public void Maximize()
        {
            Native.Manage().Window.Maximize();
        }

        public void ResizeWindowTo(int width, int height)
        {
            Browser.ResizeTo(width, height);
        }

        public void OpenNewTab()
        {
            var newTab = new Actions(Native);
            newTab.SendKeys(Keys.Control + "t").Perform();
        }

      
        public void CloseBrowser()
        {
            Native.Close();
        }
     
    }
}