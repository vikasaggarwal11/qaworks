﻿using Coypu.Drivers;
using Coypu.Drivers.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace UITests.SiteModel.Session
{
    public class CustomFirefoxProfile : SeleniumWebDriver
    {
        public CustomFirefoxProfile(Browser browser) : base(CustomProfileDriver(), browser)
        {
        }

        private static RemoteWebDriver CustomProfileDriver()
        {
            const string FolderName = "temp";
            var profile = new FirefoxProfile {EnableNativeEvents = true};
            profile.SetPreference("browser.download.folderList", 2);
            // profile.SetPreference("browser.download.manager.showWhenStarting", false);
            profile.SetPreference("browser.download.dir", FolderName);
            profile.SetPreference("browser.download.downloadDir", FolderName);
            profile.SetPreference("browser.download.defaultFolder", FolderName);
            profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv");
            return new FirefoxDriver(profile);
        }
    }
}