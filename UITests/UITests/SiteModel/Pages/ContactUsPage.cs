﻿using Coypu;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using UITests.SiteModel.Base;

namespace UITests.SiteModel.Pages
{
    class ContactUsPage : PageObject
    {
        public static string URL = ConfigurationManager.AppSettings.Get("SeleniumTestUrl")+"/Contact.aspx";

        public ContactUsPage(BrowserSession browser) : base(browser)
        {
        }

        private ElementScope Name()
        {
            return browser.FindCss("#ctl00_MainContent_NameBox");
        }
        private ElementScope Email()
        {
            return browser.FindCss("#ctl00_MainContent_EmailBox");
        }
        private ElementScope Message()
        {
            return browser.FindCss("#ctl00_MainContent_MessageBox");
        }
        private ElementScope SendButton()
        {
            return browser.FindCss("#ctl00_MainContent_SendButton");
        }

        public void SendMail(Table table)
        {
            foreach (var parameter in table.Rows[0])
            {
                switch(parameter.Key)
                {
                    case "name":
                        Name().FillInWith(parameter.Value);
                        break;
                    case "email":
                        Email().FillInWith(parameter.Value);
                        break;
                    case "message":
                        Message().FillInWith(parameter.Value);
                        break;
                    default:
                        break;
                }
            }
            SendButton().Click();
           
        }

        public string SuccessMessage()
        {
            return "success";
        }


    }
}
