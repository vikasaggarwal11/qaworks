﻿using System.Configuration;
using Coypu;
using UITests.SiteModel.Base;

namespace UITests.SiteModel.Pages
{
    public class HomePage : PageObject
    {
        public static string URL = ConfigurationManager.AppSettings.Get("SeleniumTestUrl");

        public HomePage(BrowserSession browser) : base(browser)
        {
        }

        private ElementScope ContactUsLink()
        {
            return browser.FindCss("[href='/contact.aspx']");
        }

        public void GoToContactUsPage()
        {
            ContactUsLink().Click();
        }

        //public ContactUsPage GoToContactUsPage()
        //{
        //    ContactUsLink().Click();
        //    return new ContactUsPage();
        //}
    }
}