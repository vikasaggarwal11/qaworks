﻿using System;
using System.Threading;
using Coypu;

namespace UITests.SiteModel.Utils
{
    public class WaitHelpers
    {
        public void WaitForElement(ElementScope elementToWait, int timesToWait = 6,
            int millisecondsToWaitEachTime = 1000)
        {
            bool elementFound = false;
            int timesAttempted = 1;

            while (!elementFound && timesAttempted < timesToWait)
            {
                try
                {
                    if (elementToWait.Exists())
                    {
                        elementFound = true;
                    }
                    else
                    {
                        Thread.Sleep(millisecondsToWaitEachTime);
                    }
                }
                catch (Exception)
                {
                }
                timesAttempted++;
            }

            if (!elementFound)
            {
                throw new Exception(string.Format("[FAILED] Element '{0}' Not found", elementToWait));
            }
        }

        public void WaitForElementWithPossibleError(ElementScope elementToWait, ElementScope errorElement,
            int timesToWait = 6, int millisecondsToWaitEachTime = 1000)
        {
            bool elementFound = false;
            int timesAttempted = 1;

            while (!elementFound && timesAttempted < timesToWait)
            {
                try
                {
                    if (elementToWait.Exists())
                    {
                        elementFound = true;
                    }
                    else
                    {
                        if (errorElement.Exists())
                        {
                            throw new Exception(string.Format("[FAILED] The error '{0}' was displayed",
                                errorElement.Value));
                        }

                        Thread.Sleep(millisecondsToWaitEachTime);
                    }
                }
                catch (Exception)
                {
                }
                timesAttempted++;
            }
        }

        public bool WaitForElementToHide(ElementScope elementToWait, int timesToWait = 6,
            int millisecondsToWaitEachTime = 1000)
        {
            bool elementDisplayed = true;
            int timesAttempted = 1;

            while (elementDisplayed && timesAttempted < timesToWait)
            {
                try
                {
                    if (elementToWait.Exists())
                    {
                        timesAttempted++;
                        Thread.Sleep(millisecondsToWaitEachTime);
                    }
                    else
                    {
                        elementDisplayed = false;
                    }
                }
                catch (Exception)
                {
                    elementDisplayed = false;
                }
            }

            return elementDisplayed;
        }

        public bool RefreshAndWaitForElementToHide(BrowserSession browser, ElementScope elementToWait,
            int timesToWait = 6, int millisecondsToWaitEachTime = 2000)
        {
            bool elementDisplayed = true;
            int timesAttempted = 1;
            Thread.Sleep(millisecondsToWaitEachTime);
            while (elementDisplayed && timesAttempted < timesToWait)
            {
                try
                {
                    browser.Refresh();
                    if (elementToWait.Exists())
                    {
                        browser.Refresh();
                        timesAttempted++;
                        Thread.Sleep(millisecondsToWaitEachTime);
                    }
                    else
                    {
                        elementDisplayed = false;
                    }
                }
                catch (Exception)
                {
                    elementDisplayed = false;
                }
            }

            return elementDisplayed;
        }

        public bool RefreshAndWaitForElement(BrowserSession browser, ElementScope elementToWait, int timesToWait = 6,
            int millisecondsToWaitEachTime = 2000)
        {
            bool elementFound = false;
            int timesAttempted = 1;

            while (!elementFound && timesAttempted <= timesToWait)
            {
                browser.Refresh();
                Thread.Sleep(millisecondsToWaitEachTime);

                try
                {
                    if (elementToWait.Exists())
                    {
                        elementFound = true;
                    }
                }
                catch (Exception)
                {
                }
                timesAttempted++;
            }

            return elementFound;
        }

        public void WaitForSeconds(int seconds)
        {
            Thread.Sleep(TimeSpan.FromSeconds(seconds));
        }

        public bool RefreshAndWaitForBrowserContent(BrowserSession browser, string contentToWaitFor, int timesToWait = 6,
            int millisecondsToWaitEachTime = 2000)
        {
            bool elementDisplayed = false;
            int timesAttempted = 1;
            Thread.Sleep(millisecondsToWaitEachTime);

            while (!elementDisplayed && timesAttempted < timesToWait)
            {
                try
                {
                    if (!browser.HasContent(contentToWaitFor))
                    {
                        browser.Refresh();
                        timesAttempted++;
                        Thread.Sleep(millisecondsToWaitEachTime);
                    }
                    else
                    {
                        elementDisplayed = true;
                    }
                }
                catch
                {
                }
            }

            if (!elementDisplayed)
            {
                throw new Exception(string.Format("Content '{0}' not displayed on the page", contentToWaitFor));
            }

            return elementDisplayed;
        }

        public bool DoesElementExist(ElementScope elementToWait, int timesToWait = 6,
            int millisecondsToWaitEachTime = 1000)
        {
            bool elementFound = false;
            int timesAttempted = 1;

            while (!elementFound && timesAttempted < timesToWait)
            {
                try
                {
                    if (elementToWait.Exists())
                    {
                        elementFound = true;
                    }
                    else
                    {
                        Thread.Sleep(millisecondsToWaitEachTime);
                    }
                }
                catch (Exception)
                {
                }
                timesAttempted++;
            }

            return elementFound;
        }
    }
}