﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Coypu;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using UITests.SiteModel.Utils;

namespace UITests.SiteModel.Base
{
    public abstract class PageObject
    {
        protected readonly BrowserSession browser;

        public PageObject(BrowserSession browser)
        {
            this.browser = browser;
        }

        #region Helpers

      
        /// Gets the the first value in a table for the selected column
        protected string SingleValueFromColumn(ElementScope table, string requiredColumnName)
        {
            return ValuesFromColumn(table, requiredColumnName).FirstOrDefault();
        }

        // Gets the all values in a table for the selected column
        protected IEnumerable<string> ValuesFromColumn(ElementScope table, string requiredColumnName)
        {
            //Get the required column index
            int index = GetColumnIndexFromContainer(table, "tr:first-child td", requiredColumnName);
            string columnSelectionCssSelector = string.Format("td:nth-child({0})", index + 1);

            //Get all the rows (except the first, i.e. the column headings)
            IEnumerable<SnapshotElementScope> tableRows = table.FindAllCss("tr.sbgridPlainDataRow");
            return
                tableRows.Union(table.FindAllCss("tr.sbgridPlainAlternatingDataRow"))
                    .Select(row => row.FindCss(columnSelectionCssSelector).Text)
                    .ToList();
        }

        // Gets the index of a column
        protected int GetColumnIndexFromContainer(ElementScope table, string groupSelector, string requiredColumnName)
        {
            int index = 0;
            foreach (SnapshotElementScope column in table.FindAllCss(groupSelector))
            {
                if (column.Text == requiredColumnName)
                    return index;
                index++;
            }
            return -1;
        }

        // Selects a single item on the specified HTML Select list
        [Obsolete("Use this instead: .SelectOption(value)")]
        protected void SelectSingleOptionByText(string fieldName, string optionText)
        {
            ElementScope selectList = browser.FindField(fieldName);

            SelectSingleOptionByText(selectList, optionText);
        }

        // Selects a single item on the specified HTML Select list
        [Obsolete("Use this instead: .SelectOption(value)")]
        protected void SelectSingleOptionByText(ElementScope selectList, string optionText)
        {
            try
            {
                //try this first, if it works, great...
                string selector = (string.IsNullOrEmpty(selectList.Id)) ? selectList.Name : selectList.Id;
                if (!string.IsNullOrEmpty(selector))
                {
                    browser.Select(optionText).From(selector);
                }
                else
                {
                    string optionXpath = string.Format(@"//option[text()=""{0}""]", optionText);
                    ElementScope option = selectList.FindXPath(optionXpath);
                    new WaitHelpers().WaitForElement(option, 1, 5);
                    //If an Ajax call is populating this list, give it a while
                    //selectList.Select(option.Value);
                    selectList.SelectOption(option.Value);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("Sequence contains no matching element"))
                {
                    throw;
                }
            }
        }

        protected RemoteWebDriver SwitchToNativeDriver(BrowserSession browserSession)
        {
            return ((RemoteWebDriver) browserSession.Native);
        }

        // Finds the table row element in table with id tableId containing searchText
        protected ElementScope FindRowContainingText(string tableCssLocator, string searchText)
        {
            ElementScope table = browser.FindCss(tableCssLocator);
            return FindRowContainingLinkText(table, searchText);
        }

        // Finds the table row element in table with id tableId containing searchText
        protected ElementScope FindRowContainingLinkText(ElementScope table, string searchText)
        {
            string matchString = @"//tr[td[a[text()=""{0}""]]]";
            matchString = string.Format(matchString, searchText);
            ElementScope row = table.FindXPath(matchString);

            new WaitHelpers().WaitForElement(row, 1, 30);

            return row;
        }

        // Scrolls the view port of the browser window to the top of the page
        protected void ScrollWindowToTopOfThePage()
        {
            Thread.Sleep(2000);
            ((IJavaScriptExecutor) browser.Native).ExecuteScript("scroll(0, 0)");
        }

        // Scrolls the view port down the specified number of pixels
        protected void ScrollWindow(int pixelHeight)
        {
            string scroller = string.Format("scroll(0, {0})", pixelHeight);
            ((IJavaScriptExecutor) browser.Native).ExecuteScript(scroller);
        }

        // Scrolls the view port down to the specified element
        protected void ScrollWindowToElement(ElementScope element)
        {
            var nativeElement = (IWebElement) element.Native;
            int x = nativeElement.Location.X;
            int y = nativeElement.Location.Y;
            string scroller = string.Format("scroll({0}, {1})", x, y);
            ((IJavaScriptExecutor) browser.Native).ExecuteScript(scroller);
        }

        #endregion
    }
}