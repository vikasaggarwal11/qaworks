﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using UITests.SiteModel.Pages;
using UITests.SiteModel.Session;
using UITests.TestHooks;

namespace UITests.SeleniumTests.Stepdefinitions
{
    [Binding]
    public sealed class ContactUsSteps  : Hooks
    {
        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef
        [Given(@"I am on the QAWorks Site")]
        public void GivenIAmOnTheQAWorksSite()
        {
            Navigator.Visit<HomePage>();  
        }

        [When(@"I contact QAWorks with the following information")]
        public void WhenIContactQAWorksWithTheFollowingInformation(Table table)
        {
            /* Go to contact us page from Home page */
            //Navigator.GoTo<HomePage>().GoToContactUsPage();

            /*Go to contact us page directly using url */
            Navigator.Visit<ContactUsPage>();           
            Navigator.GoTo<ContactUsPage>().SendMail(table);
        }

        [Then(@"I should get a ""(.*)"" acknowledgement")]
        public void ThenIShouldGetAAcknowledgement(string message)
        {
            Assert.AreEqual(message, Navigator.GoTo<ContactUsPage>().SuccessMessage());
        }       
    }
}
