﻿Feature: Contact Us Page  
	As an end user  
	I want a contact us page  
	So that I can find out more about QAWorks exciting services  


#Scenario: Valid Submission    
#	Given I am on the QAWorks Site    
#	Then I should be able to contact QAWorks with the following information      
#	| name    | j.Bloggs         6                        |
#	| email   | j.Bloggs@qaworks.com                      |
#	| message | please contact me I want to find out more |

Scenario: Valid Submission    
	Given I am on the QAWorks Site    
	When I contact QAWorks with the following information      
	| name               | email                | message                                   |
	| j.Bloggs         6 | j.Bloggs@qaworks.com | please contact me I want to find out more |
	Then I should get a "success" acknowledgement

@ignore
Scenario: Invalid Email Submission    
	Given I am on the QAWorks Site    
	When I contact QAWorks with the following information      
	| name               | email     | message                                   |
	| j.Bloggs         6 | wrongmail | please contact me I want to find out more |
	Then I should get an invalid email message "Invalid Email Address"

@ignore
Scenario: Empty form submission    
	Given I am on the QAWorks Site    
	When I contact QAWorks with the following information	
	| name | email | message |
	|      |       |         |
	Then I should get validation error messages
	| name                  | email                        | message                  |
	| Your name is required | An Email address is required | Please type your message |
