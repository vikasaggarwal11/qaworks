﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Coypu;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using UITests.SiteModel.Session;
using UITests.SiteModel.Base;
using UITests.SiteModel.Pages;

namespace UITests.TestHooks
{
    public class TestRunSettings
    {
        public bool EnableReport = true;
    }

     public static class ReportOnHelpers
            {
               
                public static void Report(string text)
                {
                    var testRunSettings = new TestRunSettings();

                    if (testRunSettings.EnableReport)
                    {
                        Console.WriteLine("[" + DateTime.UtcNow.Hour + ":" + DateTime.UtcNow.Minute + ":" +
                                          DateTime.UtcNow.Second + "] " + text);
                    }
                }
            }

    public class BrowserHelpers
    {
        public static string ScreenshotDirectory { get; set; }

        public string GetCurrentUrl(BrowserSession browser)
        {
            return browser.Location.ToString();
        }

        public static string TakeScreenshot(Navigator browser, string screenshotDirectory, string screenshotName)
        {
            string screenshotFilepath;

            try
            {
                screenshotFilepath = string.Format("{0}/{1}.png", screenshotDirectory, screenshotName);

                var driver = (ITakesScreenshot)browser.Native;
                driver.GetScreenshot().SaveAsFile(screenshotFilepath, ImageFormat.Png);
            }
            catch (Exception e)
            {
                screenshotFilepath = "[FAILED] Could not take screenshot. Error:" + e.Message;
            }

            return screenshotFilepath;
        }
    }


            [Binding]
            public class Hooks : Steps
            {
                protected static Navigator Navigator;

                #region Test hooks

                [BeforeFeature]
                public static void BeforeFeatureRun()
                {
                    DeleteScenarioArtefacts();
                    ReportOnHelpers.Report("Initializing Browser");
                    Navigator = new Navigator(Navigator.Driver.Firefox);
                    Navigator.Maximize();
                    string screenshotDirectory = GetScenarioArtefactPath();
                    BrowserHelpers.ScreenshotDirectory = screenshotDirectory;
                    CreateDirectory(screenshotDirectory);
                }

                [BeforeScenario]
                public static void BeforeScenario()
                {
                    ReportOnHelpers.Report("Recording scenario artefacts to " + BrowserHelpers.ScreenshotDirectory);
                    CloseAlerts();
                }

                [BeforeStep]
                public static void BeforeStep()
                {
                    Exception testException = ScenarioContext.Current.TestError;
                    //If we have a scenario error
                    if (testException != null)
                    {
                        Assert.Fail(testException.Message); //Skip the remaining steps
                    }
                }

                [AfterStep]
                public static void AfterStep()
                {
                    //TakeScreenshot();
                }

                [AfterScenario]
                public static void AfterScenario()
                {
                    TakeScreenshot();
                    CloseAlerts();
                    Exception testException = ScenarioContext.Current.TestError;
                    if (testException != null)
                    {
                        LogUrl();
                        DumpHtml();
                        ReportOnHelpers.Report("Error Artifacts in " + BrowserHelpers.ScreenshotDirectory);
                    }
                }

                [AfterFeature]
                public static void AfterFeatureRun()
                {
                    Navigator.CloseBrowser();
                    try
                    {
                        CloseAlerts();
                    }
                    catch
                    {
                    }
                }

                #endregion

                #region Helper methods

                //Dumps the html markup for current page
                private static void DumpHtml()
                {
                    string html = Navigator.Browser.FindCss("html").OuterHTML;
                    File.AppendAllText(string.Format("{0}{1}", GetScenarioArtefactPath(), "htmlDump.txt"), html);
                }



                private static string TakeScreenshot()
                {
                    string screenshotName = string.Format("{0} {1:HH-mm-ss-fff}", ScenarioContext.Current.ScenarioInfo.Title,
                        DateTime.Now);

                    return BrowserHelpers.TakeScreenshot(Navigator, BrowserHelpers.ScreenshotDirectory, screenshotName);
                }

                private static void LogUrl()
                {
                    //URL
                    if (Navigator != null && Navigator.Native != null)
                    {
                        string urlLink = string.Format(@"URL       : {0}", Navigator.Native.Url);
                        ReportOnHelpers.Report(urlLink);
                    }
                }

                protected static string GetScenarioArtefactPath()
                {
                    return string.Format(@"C:\SeleniumArtefacts\{0}\", FeatureContext.Current.FeatureInfo.Title);
                }


                private static void DeleteScenarioArtefacts()
                {
                    string targetDirectory = GetScenarioArtefactPath();
                    if (Directory.Exists(targetDirectory))
                        Directory.Delete(targetDirectory, true);
                }

                public static void CloseAlerts()
                {
                    try
                    {
                        IAlert alert = Navigator.Native.SwitchTo().Alert();

                        if (alert != null)
                        {
                            ReportOnHelpers.Report(
                                string.Format("[INFO] Attempting to close alert window with text: {0}", alert.Text));
                            alert.Accept();
                            ReportOnHelpers.Report(" > Closed open alert window");
                        }
                    }
                    catch (NoAlertPresentException)
                    {
                    }
                    catch (WebDriverException)
                    {
                    }
                }

             public static string CreateDirectory(string targetDirectory)
            {
                if (!Directory.Exists(targetDirectory))
                {
                    Directory.CreateDirectory(targetDirectory);
                }

                if (!targetDirectory.EndsWith(@"\"))
                {
                    targetDirectory += @"\";
                }

                return targetDirectory;
            }

                #endregion
            }

           
        }
  

